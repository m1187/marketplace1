<?php

use App\Http\Controllers\CatalogController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\ReviewController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('catalog', CatalogController::class);
Route::resource('country', CountryController::class);
Route::resource('review', ReviewController::class)->only('store', 'update', 'destroy');

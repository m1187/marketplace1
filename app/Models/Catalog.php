<?php

namespace App\Models;

use App\Contracts\CatalogContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Catalog extends Model
{
    use HasFactory;

    protected $fillable = [CatalogContract::FIELD_NAME, CatalogContract::FIELD_PARENT_ID];

    public function children()
    {
        return $this->hasMany(Catalog::class, CatalogContract::FIELD_PARENT_ID);
    }
}

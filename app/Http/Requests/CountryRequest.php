<?php

namespace App\Http\Requests;

use App\Contracts\CountryContract;
use Illuminate\Foundation\Http\FormRequest;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            CountryContract::FIELD_COUNTRY => ['required', 'max:255'],
            CountryContract::FIELD_ACTIVE => 'required',
        ];
    }
}

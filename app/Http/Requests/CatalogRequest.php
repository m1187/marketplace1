<?php

namespace App\Http\Requests;

use App\Contracts\Catalog;
use App\Contracts\CatalogContract;
use Illuminate\Foundation\Http\FormRequest;

class CatalogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            CatalogContract::FIELD_NAME => ['required', 'max:255'],
            CatalogContract::FIELD_PARENT_ID => [],
        ];
    }
}

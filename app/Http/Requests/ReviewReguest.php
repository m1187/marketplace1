<?php

namespace App\Http\Requests;

use App\Contracts\ReviewContract;
use Illuminate\Foundation\Http\FormRequest;

class ReviewReguest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ReviewContract::FIELD_RATING => ['required', 'digits_between:1,5'],
            ReviewContract::FIELD_COMMENT => [],
            ReviewContract::FIELD_PRODUCT_ID => [],
            ReviewContract::FIELD_USER_ID => [],
        ];
    }
}

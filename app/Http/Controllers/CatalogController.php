<?php

namespace App\Http\Controllers;

use App\Contracts\CatalogContract;
use App\Http\Requests\CatalogRequest;
use App\Models\Catalog;

class CatalogController extends Controller
{
    public function index()
    {
        $catalogs = Catalog::whereNull('parent_id')->with('children')->get();

        // $catalogs = Catalog::paginate(15)->get();
        return view('catalogs.index', ['catalogs' => $catalogs]);
    }

    public function create()
    {
        $catalogs = Catalog::get();
        return view('catalogs.create', ['catalogs' => $catalogs]);
    }

    public function store(CatalogRequest $request)
    {

        $data = $request->validated();
        Catalog::create($data);

        return back();
    }

    public function destroy(Catalog $catalog)
    {
        $catalog->delete();

        return redirect('/');
    }

    public function edit(Catalog $catalog)
    {
        return view('catalogs.edit', compact('catalog'));
    }

    public function update(Catalog $catalog, CatalogRequest $request)
    {
        $catalog->update([$request]);

        return back();
    }


}

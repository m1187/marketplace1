<?php

namespace App\Contracts;

interface CatalogContract
{
    const FIELD_NAME = 'name';
    const FIELD_PARENT_ID = 'parent_id';
}

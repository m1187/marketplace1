<?php
namespace App\Contracts;

interface CountryContract {
    const FIELD_COUNTRY = 'country';
    const FIELD_ACTIVE = 'active';
}

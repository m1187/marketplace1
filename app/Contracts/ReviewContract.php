<?php
namespace App\Contracts;

interface ReviewContract {
    const FIELD_COMMENT = 'comment';
    const FIELD_RATING = 'rating';
    const FIELD_USER_ID = 'user_id';
    const FIELD_PRODUCT_ID = 'product_id';
}

<?php

use App\Contracts\Catalog;
use App\Contracts\CatalogContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogs', function (Blueprint $table) {
            $table->id();
            $table->string(CatalogContract::FIELD_NAME);
            $table->integer(CatalogContract::FIELD_PARENT_ID)->unsigned()->nullable()->default(null);
            $table->foreign(CatalogContract::FIELD_PARENT_ID)->references('id')->on('catalogs')->onUpdate('cascade')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogs');
    }
}

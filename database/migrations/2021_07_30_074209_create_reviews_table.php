<?php

use App\Contracts\ReviewContract;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reviews', function (Blueprint $table) {
            $table->id();
            $table->integer(ReviewContract::FIELD_RATING);
            $table->longText(ReviewContract::FIELD_COMMENT)->nullable();
            $table->foreignId(ReviewContract::FIELD_USER_ID)->constrained()->onUpdate('cascade')->onDelete('set null');
            $table->foreignId(ReviewContract::FIELD_PRODUCT_ID)->constrained()->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reviews');
    }
}

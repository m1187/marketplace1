<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>MARKETPLACE</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<style>
    .checked {
        color: green;
    }
    ul{
        overflow-x:hidden;
        white-space:nowrap;

        width: 100%;
    }

    li{
        display:inline;
    }
</style>
<body>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
              <a class="navbar-brand" href="/">Home</a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div class="navbar-nav">
                  <a class="nav-link active" aria-current="page" href="{{ route('catalog.create') }}">Catalog create</a>
                  <a class="nav-link" href="{{ route('catalog.index') }}">Catalog index</a>
                  <a class="nav-link" href="{{ route('country.create') }}">Country create</a>
                </div>
              </div>
            </div>
        </nav>
        @yield('content')
        <ul>
            @for ($i = 0; $i < 5; $i++)
                <li class="fa fa-star checked" value="{{ $i }}"></li>
            @endfor
        </ul>
    </div>
</body>
</html>

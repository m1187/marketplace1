@extends('welcome')
@section('content')
    @if ($errors->any())
        <div class="alert alert-danger mt-4">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('catalog.store') }}" method="post">
        @csrf
        <div class="input-group mb-3">
            <input type="text" class="form-control mt-4" aria-label="Sizing example input" name="name" aria-describedby="inputGroup-sizing-default" placeholder="Catalog" required>
        </div>
        <select class="form-select" name="parent_id">
            <option selected value="">Null</option>
            @foreach ($catalogs as $catalog)
                <option value="{{ $catalog->id }}">
                    {{ $catalog->name }}
                </option>
            @endforeach
        </select>
        <button class="btn btn-success mt-4">CREATE</button>
    </form>
@endsection

@extends('welcome')
@section('content')
<ul>
    @foreach ($catalogs as $catalog)
        <li>{{ $catalog->name }}</li>
        <ul>
            @foreach ($catalog->children as $subCatalog)
                <li>
                    {{ $subCatalog->name }}
                </li>
                <ul>
                    @foreach ($subCatalog->children as $subSubCatalog)
                        <li>{{ $subSubCatalog->name }}</li>
                        <ul>
                            @foreach ($subSubCatalog->children as $subSubSubCatalog)
                                    <li>{{ $subSubSubCatalog->name }}</li>
                                    <ul>
                                        @foreach ($subSubSubCatalog->children as $SsubSubSubCatalog)
                                            <li>{{ $SsubSubSubCatalog->name }}</li>
                                        @endforeach
                                    </ul>

                            @endforeach
                        </ul>
                    @endforeach

                </ul>
            @endforeach
        </ul>
    @endforeach

  </ul>
@endsection
